<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['home'] = 'home/index';
$route['owners'] = 'owners/index';
$route['garages'] = 'garages/index';
//$route['post'] = 'garages/get_garages_by_owner_id';
//$route['garages/(:any)'] = 'garages/get_garages_by_owner_id/$1';
//$route['(:any)'] = 'garages/get_garages_by_owner_id/$1';


$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
