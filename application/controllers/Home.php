<?php
  class Home extends CI_Controller{

    public function index(){
      // Page Title
      $data['title'] = 'Welcome!';
      // Owners list
      $data['owners'] = $this->Owners_model->get_owners();
      //Countires list (only supported one)
      $data['countries'] = $this->Garages_model->get_countries();

      //Load views
      $this->load->view('templates/header');
      $this->load->view('home/index', $data);
      $this->load->view('templates/footer');
    }

  }// end of Home controller
?>
