<?php
  class Garages extends CI_Controller{
    /*
    Auto-load: Garages_model, Owners_model
    Helpers: url, function, form
    */

    public function index(){
      $data['title'] = 'Garages';
      $data['garages'] = $this->Garages_model->get_garages();


      $this->load->view('templates/header');
      $this->load->view('garages/index', $data);
      $this->load->view('templates/footer');
    } // end of index function!

    /*
    This functions takes parameter owner_id as post request and return json encoded data e.g
    {
    "result": true,
    "garages": [
        {
            "garage_id": "1",
            "name": "Tampere Rautatientori",
            "hourly_price": "2",
            "currency": "EUR",
            "contact_email": "testemail@testautopark.fi",
            "points": "-1.534346916776072e-290    -15674236821976.879",
            "country_id": "FI",
            "owner_id": "1",
            "owner_name": "AutoPark"
        }
      }
      OR
      {
      "result": false,
      "garages": [
          {}
        }
    */
    public function get_garages_by_owner_id(){
      $owner_id = $this->input->post('owner_id');
      $query_data = $this->Garages_model->get_garages_by_owner_id($owner_id);

      $garages = array();
      if (count($query_data)>0) {
        $result = true;
        foreach ($query_data as $key => $value) {
          $garages[$key]['garage_id']      = $value['garage_id'];
          $garages[$key]['name']           = $value['name'];
          $garages[$key]['hourly_price']   = $value['hourly_price'];
          $garages[$key]['currency']       = getCurrencySymbol($value['currency']);
          $garages[$key]['contact_email']  = $value['contact_email'];
          $garages[$key]['points']         = $value['points'];
          $garages[$key]['country_id']     = getCountiresList($value['country_id']);
          $garages[$key]['owner_id']       = $value['owner_id'];
          $garages[$key]['owner_name']     = $value['owner_name'];
        }
      }else {
        $result = false;
      }

      $data = array('data' => array('result'=> $result, 'garages' => $garages));
      $this->load->view('garages/garages', $data);

    } // end of get_garages_by_owner_id function!

    /*
      Get garages list by country_id
    */
    public function get_garages_by_country_id(){
      $country_id = $this->input->post('country_id');
      $query_data = $this->Garages_model->get_garages_by_country_id($country_id);

      $garages = array();
      if (count($query_data)>0) {
        $result = true;
        foreach ($query_data as $key => $value) {
          $garages[$key]['garage_id']      = $value['garage_id'];
          $garages[$key]['name']           = $value['name'];
          $garages[$key]['hourly_price']   = $value['hourly_price'];
          $garages[$key]['currency']       = getCurrencySymbol($value['currency']);
          $garages[$key]['contact_email']  = $value['contact_email'];
          $garages[$key]['points']         = $value['points'];
          $garages[$key]['country_id']     = getCountiresList($value['country_id']);
          $garages[$key]['owner_id']       = $value['owner_id'];
          $garages[$key]['owner_name']     = $value['owner_name'];
        }
      }else {
        $result = false;
      }

      $data = array('data' => array('result'=> $result, 'garages' => $garages));
      $this->load->view('garages/garages', $data);

    } // end of get_garages_by_country_id function!


    /*
      Get garages list by longitude and latitude
    */
    public function get_garages_by_long_lat(){
      $this->form_validation->set_rules('longitude', 'Longitude', 'required');
      $this->form_validation->set_rules('latitude', 'Latitude', 'required');

      if($this->form_validation->run() === FALSE){
        echo '* Longitude and latitude values are required';

      }else{
        $longitude = $this->input->post('longitude');
        $latitude = $this->input->post('latitude');

        $query_data = $this->Garages_model->get_garages_by_long_lat($longitude, $latitude);

        $garages = array();
        if (count($query_data)>0) {
          $result = true;
          foreach ($query_data as $key => $value) {
          
            $garages[$key]['garage_id']      = $value['garage_id'];
            $garages[$key]['name']           = $value['name'];
            $garages[$key]['hourly_price']   = $value['hourly_price'];
            $garages[$key]['currency']       = getCurrencySymbol($value['currency']);
            $garages[$key]['contact_email']  = $value['contact_email'];
            $garages[$key]['points']         = $value['point'];
            $garages[$key]['country_id']     = getCountiresList($value['country_id']);
            $garages[$key]['owner_id']       = $value['owner_id'];
            $garages[$key]['owner_name']     = $value['owner_name'];
          }
        }else {
          $result = false;
        }

        $data = array('data' => array('result'=> $result, 'garages' => $garages));
        $this->load->view('garages/garages', $data);


      }

    } // end of get_garages_by_longitude and latitude function!


    public function get_garages($owner_id){
      $data = $this->Garages_model->get_garages_by_owner_id($owner_id);
      if(empty($data['post'])){
        show_404();
      }

      $data['title'] = $data['post']['title'];

      $this->load->view('templates/header');
      $this->load->view('posts/view', $data);
      $this->load->view('templates/footer');
    }

    public function view($slug = NULL){
      $data['post'] = $this->Post_model->get_posts($slug);
      if(empty($data['post'])){
        show_404();
      }

      $data['title'] = $data['post']['title'];

      $this->load->view('templates/header');
      $this->load->view('posts/view', $data);
      $this->load->view('templates/footer');

    } // end of get_garages function!

  }// end of Garages controller


?>
