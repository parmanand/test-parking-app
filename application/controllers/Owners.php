<?php
  class Owners extends CI_Controller
  {

    public function index()
    {
      $data['title'] = 'Owners';
      $data['owners'] = $this->Owners_model->get_owners();

      $this->load->view('templates/header');
      $this->load->view('owners/index', $data);
      $this->load->view('templates/footer');
    } // end of index function!

    public function view($slug = NULL){
      $data['post'] = $this->Post_model->get_posts($slug);
      if(empty($data['post'])){
        show_404();
      }
      //echo '<pre>';print_r($data); echo '</pre>';
      $data['title'] = $data['post']['title'];

      $this->load->view('templates/header');
      $this->load->view('posts/view', $data);
      $this->load->view('templates/footer');

    } // end of view function!
  }// end of Post controller


?>
