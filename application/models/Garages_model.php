<?php
/**
 * This is the model class for table "garages".
 *
 * @property integer $garage_id
 * @property string  $name  //garage owner name
 * @property integer $hourly_price
 * @property decimal $currency
 * @property string  $country_id
 * @property integer $owner_id
 * @property point   $lat & $lng
 */
class Garages_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  /*
  Get all Garages or garages detals with garage_id
  @param interger $garage_id
  */
  public function get_garages($garage_id = FALSE){
    if($garage_id === FALSE){
      //$query = $this->db->get('garages');
      //return $query->result_array();
      $this->db->join('owners', 'owners.id = garages.owner_id');
      $query = $this->db->get('garages');
      return $query->result_array();
    }

    $query = $this->db->get_where('garages', array('id' => $garage_id));
    return $query->row_array();
  } // end of get_garages function!

  /*
  Get all Garages by owner_id
  @param interger $owner_id
  */
  public function get_garages_by_owner_id($owner_id){
    //$this->db->join('owners', 'owners.id = garages.owner_id');
    //$query = $this->db->get_where('garages', array('owner_id' => $owner_id));
    $query = "SELECT garage_id, name, hourly_price, currency, contact_email, concat(X(point), '    ' ,Y(point)) as points, country_id, owner_id, owner_name FROM garages, owners where owners.id = '$owner_id' and owners.id = garages.owner_id";
    $query = $this->db->query($query);
    return $query->result_array();
  }

  /*
  Get all Garages by country_id
  @param string $country_id
  */
  public function get_garages_by_country_id($country_id){
      $query = "SELECT garage_id, name, hourly_price, currency, contact_email, concat(X(point), '    ' ,Y(point)) as points, country_id, owner_id, owner_name FROM garages, owners where country_id = '$country_id' and owners.id = garages.owner_id";
      $query = $this->db->query($query);
      return $query->result_array();
  }

  /*
  Get all Garages by lognitude & latitude
  @param decimal lognitude
  @param decimal latitude
  */
  public function get_garages_by_long_lat($longitude, $latitude){
      $this->db->select("*, ( 3959 * acos( cos( radians($latitude) ) * cos( radians( Y(point) ) ) * cos( radians( X(`point`) ) - radians($latitude) ) + sin( radians($latitude) ) * sin( radians( Y(point) ) ) ) ) AS distance");
      $query = $this->db->get('garages, owners');
      return $query->result_array();
  }

  /*
    Select conunty ids (distinct) to only show available option.
  */
  public function get_countries(){
    $this->db->distinct();
    $this->db->select('country_id');
    $query = $this->db->get('garages');
    return $query->result_array();
  }

} // end of Garages_model!
?>
