<?php
/**
 * This is the model class for table "onwers".
 *
 * @property integer $id
 * @property string $owner_name
 * @property string $contact_email
 */

class Owners_model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  /*
  Get all Owners list or one onwer by owner_id
  @param integer $owner_id
  */
  public function get_owners($owner_id = FALSE){
    if($owner_id === FALSE){
      $query = $this->db->get('owners');
      return $query->result_array();
    }

    $query = $this->db->get_where('owners', array('id' => $owner_id));
    return $query->row_array();
  } // end of get_owners function!

} // end of Owners_model!
?>
