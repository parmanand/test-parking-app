<h2><?php echo $title; ?></h2>
<br />
<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Id:</th>
      <th scope="col">Owner Name:</th>
      <th scope="col">Contact Email:</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($owners as $owner):?>
      <tr>
        <th scope="row"><?php echo $owner['id']; ?></th>
        <td><h5><?php echo $owner['owner_name'];?></h5></td>
        <td><?php echo $owner['contact_email'];?></td>
      </tr>
    <?php endforeach;?>
  </tbody>
</table>
