<?php
/*
Print json encoded array!
*/
header('Content-Type: application/json; charset=ISO-8859-1');
echo json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
?>
