<h2><?php echo $title; ?></h2>
<br />
<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Id:</th>
      <th scope="col">Garage Name:</th>
      <th scope="col">Garage Owner Name:</th>
      <th scope="col">Hourly Price:</th>
      <th scope="col">Currency:</th>
      <th scope="col">Country:</th>
      <th scope="col">Point:</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($garages as $garage): ?>
      <tr>
        <th scope="row"><?php echo $garage['garage_id']; ?></th>
        <td><h5><?php echo $garage['name']; ?></h5></td>
        <td><?php echo $garage['owner_name']; ?></td>
        <td><?php echo $garage['hourly_price']; ?></td>
        <td><?php echo getCurrencySymbol($garage['currency']); ?></td>
        <td><?php echo getCountiresList($garage['country_id']); ?></td>
        <td><?php echo $garage['point'];?></td>
      </tr>

    <?php endforeach;?>
  </tbody>
</table>
