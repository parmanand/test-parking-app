<div class="jumbotron">
  <h1 class="display-3"><?php echo $title; ?></h1>
  <hr class="my-4">

  <div id="message" style="display:none;">test</div>
  <div>
    <?php echo form_open("garages/get_garages_by_owner_id"); ?>
      <legend>Data by Grage Owner: </legend>
        <select class="custom-select" id="owner_id" name="owner_id">
          <option value="default" selected="">Open this select menu</option>
          <?php foreach ($owners as $owner): ?>
            <option value="<?php echo $owner['id']; ?>"><?php echo $owner['owner_name'];?></option>
          <?php endforeach;?>
        </select>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <?php echo form_open("garages/get_garages_by_country_id"); ?>
      <legend>Data by Country: </legend>
        <select class="custom-select" id="country_id" name="country_id">
          <option value="default" selected="">Open this select menu</option>
          <?php foreach ($countries as $countries): ?>
            <option value="<?php echo $countries['country_id']; ?>"><?php echo getCountiresList($countries['country_id']);?></option>
          <?php endforeach;?>
        </select>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <?php echo validation_errors(); ?>
    <?php echo form_open("garages/get_garages_by_long_lat"); ?>
    <legend>Search by Longitude & Latitude: </legend>
      <div class="form-group">
        <label for="Longitude">Longitude</label>
        <input type="text" class="form-control" id="longitude" name="longitude">
      </div>
      <div class="form-group">
        <label for="Latitude">Latitude</label>
        <input type="text" class="form-control" id="latitude" name="latitude">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>

  </div>
</div>
