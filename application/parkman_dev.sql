-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2018 at 11:29 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parkman_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `garages`
--

CREATE TABLE `garages` (
  `garage_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `hourly_price` float NOT NULL,
  `currency` varchar(5) NOT NULL,
  `country_id` varchar(2) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `point` point NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `garages`
--

INSERT INTO `garages` (`garage_id`, `name`, `hourly_price`, `currency`, `country_id`, `owner_id`, `point`) VALUES
(1, 'Tampere Rautatientori', 2, 'EUR', 'FI', 1, '\0\0\0\0\0\0\0}Â²#Ã±â€N@Â Ã…Ã„ÃžÂ¯Ã®8@'),
(2, 'Punavuori Garage', 1.5, 'EUR', 'FI', 1, '\0\0\0\0\0\0\0CÃ‰Ã¤Ã”ÃŽN@!Ã§Ã½Ã°8@'),
(3, 'Unknown', 3, 'EUR', 'FI', 1, '\0\0\0\0\0\0\0bÃ MÂ²N@F>Ã†q,Ã°8@'),
(4, 'Fitnesstukku', 3, 'EUR', 'FI', 1, '\0\0\0\0\0\0\0,xoÃ¨%N@\0\0\0Â°tÃ¯8@'),
(5, 'Kauppis', 3, 'EUR', 'FI', 1, '\0\0\0\0\0\0\0Ã†]lÃ¹N@nÃ›Â¶	Ã­Ã«8@'),
(6, 'Q-Park1', 2, 'EUR', 'FI', 2, '\0\0\0\0\0\0\0Ã®I=â€”N@`Ã¼Ã‚(Ã®8@');

-- --------------------------------------------------------

--
-- Table structure for table `owners`
--

CREATE TABLE `owners` (
  `id` int(11) NOT NULL,
  `owner_name` varchar(55) NOT NULL,
  `contact_email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owners`
--

INSERT INTO `owners` (`id`, `owner_name`, `contact_email`) VALUES
(1, 'AutoPark', 'testemail@testautopark.fi'),
(2, 'Q-park', 'testemail@testautopark.fi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `garages`
--
ALTER TABLE `garages`
  ADD PRIMARY KEY (`garage_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `owner_id` (`owner_id`);

--
-- Indexes for table `owners`
--
ALTER TABLE `owners`
  ADD PRIMARY KEY (`owner_name`),
  ADD UNIQUE KEY `owner_id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `garages`
--
ALTER TABLE `garages`
  MODIFY `garage_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `owners`
--
ALTER TABLE `owners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
